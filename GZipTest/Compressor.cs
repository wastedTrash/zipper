﻿using System;
using System.Threading;
using System.IO;
using System.IO.Compression;

namespace GZipTest
{
    class Compressor : GZip
    {   
        /// <summary>
        /// Initialize new instance of class Compressor
        /// </summary>
        /// <param name="input">Path to file which should be compressed</param>
        /// <param name="output">Path to file where to write compressed file</param>
        public Compressor(string input, string output) : base() {
            _fileManager = new FileManager(input, output, compress: true);
        }

        /// <summary>
        /// Launches activity of instance of class GZip. Prepare and start work of threads
        /// </summary>
        protected override void StartThreads() {
            try {
                _readThread = new Thread(new ThreadStart(_fileManager.ReadFile));
                _readThread.Start();
                for (int i = 0; i < _compressThreads.Length; i++) {
                    _compressThreads[i] = new MutableTuple<Thread, bool>(new Thread(Compress), false);
                    _compressThreads[i].Item1.Start(i);
                }
                _writeThread = new Thread(new ThreadStart(_fileManager.WriteIntoFile));
                _writeThread.Start();
                _writeThread.Join();
            }
            catch (Exception ex) {
                Console.WriteLine("Error is occured!\n Method: {0}\n Error description {1}", ex.TargetSite, ex.Message);
                _cancelled = true;
            }
        }

        /// <summary>
        /// Compresses blocks of data. Gets block from queue of reading, compresses it 
        /// and writes results into queue of writing
        /// </summary>
        /// <param name="i">The number of thread where function is executed</param>
        private void Compress(object i) {
            int threadIndex = (int) i;
            try {
                // Executes until nothing will be enqueued into queue of reading and queue of reading will be empty
                while ((_fileManager.queueReader.WillBeExtended || _fileManager.queueReader.GetCount() != 0)  && !_cancelled && !_fileManager.IsCancelled) {
                    ByteBlock block = _fileManager.queueReader.Dequeue();
                    if (block == null)
                        continue;
                    ByteBlock outBytes;
                    using (MemoryStream compressedMemStream = new MemoryStream()) {
                        using (GZipStream zipStream = new GZipStream(compressedMemStream, CompressionMode.Compress))
                            // Compresses data with help of GZipStream into the block
                            zipStream.Write(block.Data, 0, block.Data.Length);
                        outBytes = new ByteBlock(block.ID, compressedMemStream.ToArray());
                    }
                    _fileManager.queueWriter.Enqueue(outBytes);
                }
            } catch (Exception ex) {
                Console.WriteLine("Error in compressing thread. \n Description: {0}", ex.Message);
                _cancelled = true;
            }
            // Set finish flag 
            _compressThreads[threadIndex].Item2 = true;
            if (IsAllThreadsDone()) {
                _fileManager.queueWriter.WillBeExtended = false;
                if (_cancelled && !_fileManager.IsCancelled)
                    Cancel();
            }
            return;
        }
    }
}