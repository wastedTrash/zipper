﻿using System;
using System.Threading;

namespace GZipTest
{
  
   public abstract class GZip
    {   /// <summary>
        /// Mutable Tuple
        /// </summary>
        /// <typeparam name="T1">First item Type</typeparam>
        /// <typeparam name="T2">Second item Type</typeparam>
        public class MutableTuple<T1, T2>
        {
            public T1 Item1 { get; set; }
            public T2 Item2 { get; set; }

            public MutableTuple(T1 item1, T2 item2) {
                Item1 = item1;
                Item2 = item2;
            }
        }

        static public int THREADS_COUNT = Environment.ProcessorCount;
        // Threads for compressing and finish flags for each one
        protected MutableTuple<Thread, bool>[] _compressThreads;
        protected Thread _writeThread, _readThread;
        protected bool _cancelled;
        protected bool _success;
        /// <summary>
        /// Object to work with files. E.g: read a file or an archive, write data into file, etc.
        /// </summary>
        protected FileManager _fileManager;

        /// <summary>
        /// Initializes new instance of class GZip
        /// </summary>
        public GZip() {
            _cancelled = _success = false;
            _compressThreads = new MutableTuple<Thread, bool>[THREADS_COUNT];
        }

        /// <summary>
        /// Returns results of process with int value
        /// </summary>
        /// <returns>Returns 0 if process is successfully finished.
        /// Returns 1 if process is cancelled or there was an error.</returns>
        public int CallBackResult() {
            if (!_cancelled && _success) {
                Console.WriteLine("Process is successfully finished!");
                return 0;
            }
            return 1;
        }

        /// <summary>
        /// Cancels all activity of instance of class GZip
        /// </summary>
        public void Cancel()  {
            _fileManager.Cancel();
            if (!_cancelled)
                _cancelled = true;
        }

        /// <summary>
        /// Launches activity of instance of class GZip. Prepares and starts the work of threads
        /// </summary>
        public void Launch() {
            Console.WriteLine(@"{0} is working...", this.GetType().Name);
            StartThreads();
            if (!_cancelled)
                _success = true;
        }

        /// <summary>
        /// Returns whether all threads for compressing finished their work
        /// </summary>
        /// <returns>true if all threads are finished.
        /// false if at least one thread is still working</returns>
        protected bool IsAllThreadsDone() {
            foreach (MutableTuple<Thread, bool> item in _compressThreads)
                if ((item != null) && (!item.Item2))
                    return false;
            return true;
        }

        /// <summary>
        /// Prepares and starts the work of threads for reading file, compressing/decompressing data, writing data into file
        /// </summary>
        protected abstract void StartThreads();

    }
}
