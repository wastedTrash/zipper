﻿using System;
using System.Linq;
using System.Threading;
using System.IO;
using System.IO.Compression;

namespace GZipTest
{
    class Decompressor : GZip {

        /// <summary>
        /// Initializes new instance of class Decompressor
        /// </summary>
        /// <param name="input">Path to file which should be decompressed</param>
        /// <param name="output">Path to file where to write decompressed data</param>
        public Decompressor(string input, string output) : base() {
            _fileManager = new FileManager(input, output, compress: false);
        }
        /// <summary>
        /// Launches activity of instance of class GZip. Prepare and start work of threads
        /// </summary>
        protected override void StartThreads() {
            _readThread = new Thread(new ThreadStart(_fileManager.ReadArchive));
            _readThread.Start();
            for (int i = 0; i < _compressThreads.Length; i++) {
                _compressThreads[i] = new MutableTuple<Thread, bool>(new Thread(Decompress), false);
                _compressThreads[i].Item1.Start(i);
            }
            _writeThread = new Thread(new ThreadStart(_fileManager.WriteIntoFile));
            _writeThread.Start();
            _writeThread.Join();
        }

        /// <summary>
        /// Decompresses blocks of data. Gets compressed block from queue of reading, decompresses it 
        /// and writes results into queue of writing
        /// </summary>
        /// <param name="i">The number of thread where function is executed</param>
        private void Decompress(object i) {
            int threadIndex = (int) i;
            int count = 0;
            try {
                ByteBlock block;
                // Executes until nothing will be enqueued into queue of reading and queue of reading will be empty
                while ((_fileManager.queueReader.WillBeExtended || _fileManager.queueReader.GetCount() != 0) && !_cancelled && !_fileManager.IsCancelled) {
                    ++count;
                    block = _fileManager.queueReader.Dequeue();
                    if (block == null)
                        continue;
                    using (MemoryStream ms = new MemoryStream(block.CompressedData)) {
                        using (GZipStream zipStream = new GZipStream(ms, CompressionMode.Decompress)) {
                            // Decompresses data with help of GZipStream into the block
                            zipStream.Read(block.Data, 0, block.Data.Length);
                            byte[] decompressedData = block.Data.ToArray();
                            block = new ByteBlock(block.ID, decompressedData);
                            _fileManager.queueWriter.Enqueue(block);
                        }
                    }
                }
            } catch (Exception ex) {
                Console.WriteLine("Error in decompressing thread. \n Description: {0}", ex.Message);
                _cancelled = true;
            }
            // Set finish flag 
            _compressThreads[threadIndex].Item2 = true;
            if (IsAllThreadsDone()) {
                _fileManager.queueWriter.WillBeExtended = false;
                if (_cancelled && !_fileManager.IsCancelled)
                    Cancel();
            }
            return;
        }
    }
}
 