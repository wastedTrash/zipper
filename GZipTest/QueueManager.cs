﻿using System.Collections.Generic;
using System.Threading;

namespace GZipTest
{
    /// <summary>
    /// Class for block of bytes, containing data
    /// </summary>
    public class ByteBlock
    {
        private int _id;
        private byte[] _data;
        private byte[] _compressedData;

        public int ID { get { return _id; } }
        public byte[] Data {
            set { _data = value; }
            get { return _data; }
        }
        public byte[] CompressedData {
            set { _compressedData = value;  }
            get { return _compressedData; }
        }

        public ByteBlock(int id, byte[] data) : this(id, data, new byte[0]) { }
        public ByteBlock(int id, byte[] data, byte[] compressedData) {
            _id = id;
            _data = data;
            _compressedData = compressedData;
        }
    }
    /// <summary>
    /// Class to handle multithread queueing
    /// </summary>
    public class QueueManager {
        private bool _willBeExtended;
        private int _maxSize;
        private int _blocksCount;
        private object _locker;

        private Queue<ByteBlock> queue = new Queue<ByteBlock>();
        /// <summary>
        /// Contains flag will any items be enqueued 
        /// </summary>
        public bool WillBeExtended {
            set {
                lock (_locker) {
                    _willBeExtended = value;
                    Monitor.PulseAll(_locker);
                }
            }
            get { return _willBeExtended; }
        }

        public QueueManager() : this(500) {}
        /// <summary>
        /// Initializes new instance of class QueueManager
        /// </summary>
        /// <param name="maxSize">Maximum size of queue</param>
        public QueueManager(int maxSize) {
            _willBeExtended = true;
            _maxSize = maxSize;
            _blocksCount = 0;
            _locker = new object();
            queue = new Queue<ByteBlock>(_maxSize);
        }
        /// <summary>
        /// Enqueues block
        /// </summary>
        /// <param name="block">Block of data</param>
        public void Enqueue(ByteBlock block) {
            // Wait until queue will be diminished
            lock (_locker) {
                // Wait block's turn to be enqueued
                while ((block.ID != _blocksCount)) {
                    if (!_willBeExtended) {
                        Monitor.PulseAll(_locker);
                        return;
                    }
                    Monitor.Wait(_locker);
                }
                if (queue.Count == _maxSize) {
                    Monitor.Wait(_locker);
                }
                queue.Enqueue(block);
                _blocksCount++;
                // inform everyone who uses this manager that 
                // they can modify queue
                Monitor.PulseAll(_locker);
            }
        }

        /// <summary>
        /// Dequeues from the queue
        /// </summary>
        /// <returns>Returns block of data</returns>
        public ByteBlock Dequeue() {
            ByteBlock block = null;
            lock (_locker) {
                while (queue.Count == 0) {
                    if (!_willBeExtended) {
                        Monitor.PulseAll(_locker);
                        return block;
                    }
                    Monitor.Wait(_locker);
                }
                if (queue.Count != 0)
                    block = queue.Dequeue();
                Monitor.PulseAll(_locker);
                return block;
            }
        }
        /// <summary>
        /// Returns the number of items in the queue
        /// </summary>
        /// <returns>The number of items in the queue</returns>
        public int GetCount() {
            return queue.Count;
        }
        /// <summary>
         /// Returns the number of items in the queue
         /// </summary>
         /// <returns>The number of items in the queue</returns>
        public void Stop() {
            lock (_locker)
                Monitor.PulseAll(_locker);
        }
    }
}