﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace GZipTest
{
    /// <summary>
    /// Class for validating input arguments
    /// </summary>
    public static class Validator {
        public static string[] zipCommands = { "compress", "-c", "zip" };
        public static string[] unzipCommands= { "decompress", "-d", "unzip" };
        public static string[] helpCommands = { "help", "-h", "?" };
        /// <summary>
        /// Checks whether input arguments are correct
        /// </summary>
        /// <param name="args">Arguments came as parameters into the program</param>
        public static void ValidateArgs(string[] args) {
            if (args.Length == 0) {
                Console.WriteLine("Please enter valid arguments. To show \"help\" launch program with \"-h\" parameter.");
                Program.ShowInfo();
                throw new Exception("Arguments haven't been specified.");
            }

            if (Array.IndexOf(helpCommands, args[0].ToLower()) > -1)
                return;

            if (Array.IndexOf(zipCommands, args[0].ToLower()) < 0 && Array.IndexOf(unzipCommands, args[0].ToLower()) < 0)
                throw new Exception("First argument should be:\n To compress: \"compress\", \"-c\", \"zip\". \n To decompress: \"decompress\", \"-d\", \"unzip\".");

            if (args.Length < 2 || args[1].Length == 0) {
                Program.ShowInfo();
                throw new Exception("No source file name was specified.");
            }

            if (args.Length < 3 || args[2].Length == 0) {
                Program.ShowInfo();
                throw new Exception("No destination file name was specified.");
            }

            if (args[1] == args[2])
                throw new Exception("Source and destination files should be different.");
            
            ValidateFiles(args);

            if (args.Length > 3)
                Console.WriteLine("WARNING: Only first three arguments are accepted.");
        }
        /// <summary>
        /// Checks whether files exist or reference to existed files
        /// </summary>
        /// <param name="args">Strings contain path to source and destination files</param>
        private static void ValidateFiles(string[] args) {
            FileInfo inFile = new FileInfo(args[1]);
            FileInfo outFile = new FileInfo(args[2]);

            if (!inFile.Exists)
                throw new Exception("There is no such source file. Check the path to the source file and try again.");

            if (outFile.Exists)
                throw new Exception("Destination file already exists. Please indiciate the different file name.");

            if (outFile.Extension != ".gz" && Array.IndexOf(zipCommands, args[0].ToLower()) > -1)
                throw new Exception("File to be decompressed should have .gz extension.");

            if (inFile.Extension == ".gz" && Array.IndexOf(zipCommands, args[0].ToLower()) > -1)
                throw new Exception("File has already been compressed.");
        }
    }
}
