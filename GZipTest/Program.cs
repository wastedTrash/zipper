﻿using System;

namespace GZipTest
{
    // SystemOutOfMemo
    // Freezes after cancellation
    class Program
    {
        static GZip zipper;

        static int Main(string[] args) {
            try {
                Validator.ValidateArgs(args);
                switch (args[0].ToLower()) {
                    case "compress":
                    case "-c":
                    case "zip":
                        zipper = new Compressor(args[1], args[2]);
                    break;
                    case "decompress":
                    case "-d":
                    case "unzip":
                        zipper = new Decompressor(args[1], args[2]);
                    break;
                    case "help":
                    case "-h":
                    case "?":
                        ShowInfo();
                        return 1;
                }
                Console.CancelKeyPress += new ConsoleCancelEventHandler(CancelKeyPress);
                zipper.Launch();
                return zipper.CallBackResult();
            } catch (Exception ex) {
                Console.WriteLine("\nNothing to worry about. Error has occured.\n Method: {0}\n Error description: {1}", ex.TargetSite, ex.Message);
                return 1;
            }
        }
        /// <summary>
        /// Shows instruction how to use program
        /// </summary>
        public static void ShowInfo() {
            Console.WriteLine("\nTo zip or unzip files please proceed with the following pattern to type in:\n" +
                                "Compress commands: \"compress\" or \"-c\" or \"zip\".\n" +
                                "Decompress commands:\"decompress\" or \"-d\" or \"unzip\".\n" +
                                "Compressing: GZipTest.exe [compress_command] [Source file path] [Destination file path]\n" +
                                "Deompressing: GZipTest.exe [decompress_command] [Compressed file path] [Destination file path]\n" +
                                "To cancel the execution of the program press the combination CTRL + C");
        }

        /// <summary>
        /// Handle cancel event
        /// </summary>
        /// <param name="sender">Sender of evenet</param>
        /// <param name="_args">Arguments of event</param>
        static void CancelKeyPress(object sender, ConsoleCancelEventArgs _args) {
            if (_args.SpecialKey == ConsoleSpecialKey.ControlC) {
                Console.WriteLine("\nCancelling...");
                zipper.Cancel();
            }
        }


    }
}
